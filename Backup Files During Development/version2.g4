grammar version2;

start : '[' (program)+ ']' ;

program : 'function' NAME '(' parameter (',' parameter)* ')' block
		| 'function' NAME '('')' block
		| declSt
		;

parameter : 'int' NAME
			| 'bool' NAME
			;

block : '{' (statement)+ '}';

statement : declSt 
		| assgSt
		| ifSt
		| whileSt
		|printSt
		|stackFunc
		;

declSt : intDecl
		| boolDecl
		;
		
intDecl : 'int' NAME '=' NUM {System.out.print($NUM.text);};
		
boolDecl : 'bool' NAME '=' VAL;
 
whileSt : 'while' '(' expr ')' block ;

ifSt : 'if' '(' expr ')' block {System.out.println($expr.text);}
		| 'if' '(' expr ')' block 'else' block
		;

assgSt : NAME '=' expr;

printSt : 'print' '(' NAME ')'
		| 'print' '(' '"' NAME '"' ')'
		;

stackFunc : 'stack.push(' NUM ')'
			| 'stack.top()'
			| 'stack.pop()'
			;

expr : NUM
		| NAME
		| VAL
		| expr op expr
		;
		
VAL : 'true'
		| 'false'
		;

NUM : [0-9]+ ;		

NAME : [a-zA-Z]+ ;

op : '+'
	| '-'
	| '*'
	| '=='
	| '/'
	;

WS : [ \t\r	\n]+ -> skip;