// Generated from integer.g4 by ANTLR 4.2
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link integerParser}.
 */
public interface integerListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link integerParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(@NotNull integerParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link integerParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(@NotNull integerParser.StartContext ctx);
}