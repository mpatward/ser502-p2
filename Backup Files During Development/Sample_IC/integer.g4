grammar integer;

start : 'int' ID '=' NO 
		;
ID : [a-zA-Z]+;  #abc
NO : [0-9]+;
WS : [ \t\r	\n]+ -> skip;