import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.InputStream;

public class integer {
    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        integerLexer lexer = new integerLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        integerParser parser = new integerParser(tokens);
        ParseTree tree = parser.start(); // parse

        intVisitor eval = new intVisitor();
        eval.visit(tree);
    }
}