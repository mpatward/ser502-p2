import org.stringtemplate.v4.*;

public class update_one_visitor extends update_oneBaseVisitor<Integer>{
	
	@Override 
	public Integer visitPstart(update_oneParser.PstartContext ctx) { 
	
		super.visitPstart(ctx);
		ST temp = new ST("PSTART");
		System.out.println(temp.render());
		return 0;
	}
	
	@Override 
	public Integer visitPend(update_oneParser.PendContext ctx) {
		
		super.visitPend(ctx);
		ST temp = new ST("PEND");
		System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitIntDecl(update_oneParser.IntDeclContext ctx) { 
		
		super.visitIntDecl(ctx);
		ST temp = new ST("IDEF <name>,<number>");
		String id = ctx.NAME().getText();
		String no = ctx.NUM().getText();
		temp.add("name", id);
		temp.add("number", no);
		System.out.println(temp.render());
		return 0; 
	}
	
	@Override 
	public Integer visitBoolDecl(update_oneParser.BoolDeclContext ctx) { 
	
		
		super.visitBoolDecl(ctx);
		ST boolDecl = new ST("BDEF <name>,<value>");
		String id = ctx.NAME().getText();
		String value = ctx.VAL().getText();
		boolDecl.add("name", id);
		boolDecl.add("value", value);
		System.out.println(boolDecl.render());
		return 0; 
		
	}
	
	@Override 
	public Integer visitAssg_int(update_oneParser.Assg_intContext ctx) { 
		
		
		super.visitAssg_int(ctx);
		ST assg_int = new ST("ASSGN <name>,<val>");
		String id = ctx.NAME().getText();
		String val = ctx.NUM().getText();
		assg_int.add("name", id);
		assg_int.add("val", val);
		System.out.println(assg_int.render());
		return 0; 
	}
		
	@Override 
	public Integer visitAssg_bool(update_oneParser.Assg_boolContext ctx) { 
	
		super.visitAssg_bool(ctx);
		ST bool_int = new ST("ASSGN <name>,<val>");
		String id = ctx.NAME().getText();
		String val = ctx.VAL().getText();
		bool_int.add("name", id);
		bool_int.add("val", val);
		System.out.println(bool_int.render());
		return 0;
	
	}	
	
	@Override 
	public Integer visitPrint(update_oneParser.PrintContext ctx) { 
	
		super.visitPrint(ctx);
		ST print_stmt = new ST("PRINT <name>");
		String id = ctx.NAME().getText();
		print_stmt.add("name", id);
		System.out.println(print_stmt.render());
		return 0; 
		
	}
	
	@Override 
	public Integer visitPrints(update_oneParser.PrintsContext ctx) { 
		
		super.visitPrints(ctx);
		ST print_stmt = new ST("PRINTS <name>");
		String id = ctx.NAME().getText();
		print_stmt.add("name", id);
		System.out.println(print_stmt.render());
		return 0; 
		
	}
	
	
	@Override 
	public Integer visitFnoparam(update_oneParser.FnoparamContext ctx) {
		
		super.visitFnoparam(ctx); 
		ST temp = new ST("DECL <name> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("name", id);
		System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitFparam(update_oneParser.FparamContext ctx) { 
	
		super.visitFparam(ctx);
		ST temp = new ST("DECL <name>");
		String id = ctx.NAME().getText();
		temp.add("name", id);
		System.out.print(temp.render());
		return 0;
		
	}
	
	@Override 
	public Integer visitProgram_no_params(update_oneParser.Program_no_paramsContext ctx) { 
	
		super.visitProgram_no_params(ctx);
		ST temp = new ST("FEND");
		System.out.println(temp.render());
		return 0;
	}
	
	@Override
	public Integer visitProgram_params(update_oneParser.Program_paramsContext ctx) { 
		
		super.visitProgram_params(ctx);
		ST temp = new ST("FEND");
		System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitInteger_parameter(update_oneParser.Integer_parameterContext ctx) {
		
		super.visitInteger_parameter(ctx);
		ST temp = new ST(":int <var> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("var", id);
		System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitBoolean_parameter(update_oneParser.Boolean_parameterContext ctx) { 
		
		super.visitBoolean_parameter(ctx);
		ST temp = new ST(":bool <var> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("var", id);
		System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitExpr_assg(update_oneParser.Expr_assgContext ctx) { 
		
		super.visitExpr_assg(ctx);
		ST temp = new ST("<result> \n");
		String res = ctx.NAME().getText();
		temp.add("result", res);
		System.out.print(temp.render());
		return 0; 
	
	}
	
	@Override 
	public Integer visitExpr_num(update_oneParser.Expr_numContext ctx) { 
		
		super.visitExpr_num(ctx);
		ST temp = new ST("<num> ");
		String num = ctx.NUM().getText();
		temp.add("num", num);
		System.out.print(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitExpression(update_oneParser.ExpressionContext ctx) { 
	
		super.visitExpression(ctx);
		ST temp = new ST("<operation> ");
		String op = ctx.OP().getText();
		if(op.equals("+")){
			op = "ADD";
		}
		else if(op.equals("-")){
			op = "SUB";
		}
		else if(op.equals("*")){
			op = "MUL";
		}
		else{
			op = "DIV";
		}
		temp.add("operation", op);
		System.out.print(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitIf_main(update_oneParser.If_mainContext ctx) { 
		
		super.visitIf_main(ctx);
		ST temp = new ST("IF");
		System.out.print(temp.render());
		return 0; 
		
	}
	
	@Override 
	public Integer visitExpr_comp(update_oneParser.Expr_compContext ctx) { 
		
		super.visitExpr_comp(ctx);
		ST temp = new ST(" <var1> <op> <var2>");
		String var1 = ctx.val_one().getText();
		String op = ctx.OPC().getText();
		String var2 = ctx.val_two().getText();
		if(op.equals("==")){
			op = "EQLS";
		}
		else if(op.equals("!=")){
			op = "NEQLS";
		}
		else if(op.equals("<")){
			op = "LT";
		}
		else if(op.equals(">")){
			op = "GT";
		}
		else if(op.equals("<=")){
			op = "LTE";
		}
		else{
			op = "GTE";
		}
		temp.add("var1", var1);
		temp.add("var2", var2);
		temp.add("op", op);
		System.out.println(temp.render());
		return 0; 
		
	}
	
	/*@Override
	public Integer visitIf_stmnt(update_oneParser.If_stmntContext ctx) { 
		
		super.visitIf_stmnt(ctx);
		ST temp = new ST("\nENDIF");
		System.out.println(temp.render());
		return 0; 
	}*/
	
	@Override
	public Integer visitElse_main(update_oneParser.Else_mainContext ctx) {
		
		super.visitElse_main(ctx);
		ST temp = new ST("ELSE");
		System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitIfelse_stmnt(update_oneParser.Ifelse_stmntContext ctx) { 
		
		super.visitIfelse_stmnt(ctx);
		ST temp = new ST("ENDIF");
		System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitWhileSt(update_oneParser.WhileStContext ctx) { 
		
		super.visitWhileSt(ctx);
		ST temp = new ST("ENDWHILE");
		System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitWhile_main(update_oneParser.While_mainContext ctx) { 
		
		super.visitWhile_main(ctx);
		ST temp = new ST("WHILE");
		System.out.print(temp.render());
		return 0; 
		
	}
}