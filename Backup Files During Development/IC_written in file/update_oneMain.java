import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.InputStream;

public class update_oneMain {
    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        update_oneLexer lexer = new update_oneLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        update_oneParser parser = new update_oneParser(tokens);
        ParseTree tree = parser.start(); // parse

        update_one_visitor eval = new update_one_visitor();
        eval.visit(tree);
    }
}