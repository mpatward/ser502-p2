// Generated from update_one.g4 by ANTLR 4.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class update_oneLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__12=1, T__11=2, T__10=3, T__9=4, T__8=5, T__7=6, T__6=7, T__5=8, T__4=9, 
		T__3=10, T__2=11, T__1=12, T__0=13, STARTB=14, ENDB=15, COMMA=16, BLOCKSTART=17, 
		BLOCKEND=18, OPC=19, EQ=20, PRINT=21, QUOTE=22, VAL=23, TRUE=24, FALSE=25, 
		NUM=26, NAME=27, OP=28, ADD=29, SUB=30, MUL=31, DIV=32, WS=33;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"']'", "'bool'", "'function'", "'func_call'", "'while'", "'['", "'if'", 
		"'int'", "'stack.top()'", "'stack.pop()'", "'fact'", "'stack.push('", 
		"'else'", "'('", "')'", "','", "'{'", "'}'", "OPC", "'='", "'print'", 
		"'\"'", "VAL", "'true'", "'false'", "NUM", "NAME", "OP", "'+'", "'-'", 
		"'*'", "'/'", "WS"
	};
	public static final String[] ruleNames = {
		"T__12", "T__11", "T__10", "T__9", "T__8", "T__7", "T__6", "T__5", "T__4", 
		"T__3", "T__2", "T__1", "T__0", "STARTB", "ENDB", "COMMA", "BLOCKSTART", 
		"BLOCKEND", "OPC", "EQ", "PRINT", "QUOTE", "VAL", "TRUE", "FALSE", "NUM", 
		"NAME", "OP", "ADD", "SUB", "MUL", "DIV", "WS"
	};


	public update_oneLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "update_one.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2#\u00f6\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22"+
		"\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24"+
		"\u00c0\n\24\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\30\3\30"+
		"\5\30\u00ce\n\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\33\6\33\u00dc\n\33\r\33\16\33\u00dd\3\34\6\34\u00e1\n\34\r\34\16\34"+
		"\u00e2\3\35\3\35\3\35\3\35\5\35\u00e9\n\35\3\36\3\36\3\37\3\37\3 \3 \3"+
		"!\3!\3\"\3\"\3\"\3\"\2\2#\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32"+
		"\63\33\65\34\67\359\36;\37= ?!A\"C#\3\2\5\3\2\62;\4\2C\\c|\5\2\13\f\17"+
		"\17\"\"\u0100\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2"+
		"\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2"+
		"\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2"+
		"\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2"+
		"\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3"+
		"\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\3E\3\2\2"+
		"\2\5G\3\2\2\2\7L\3\2\2\2\tU\3\2\2\2\13_\3\2\2\2\re\3\2\2\2\17g\3\2\2\2"+
		"\21j\3\2\2\2\23n\3\2\2\2\25z\3\2\2\2\27\u0086\3\2\2\2\31\u008b\3\2\2\2"+
		"\33\u0097\3\2\2\2\35\u009c\3\2\2\2\37\u009e\3\2\2\2!\u00a0\3\2\2\2#\u00a2"+
		"\3\2\2\2%\u00a4\3\2\2\2\'\u00bf\3\2\2\2)\u00c1\3\2\2\2+\u00c3\3\2\2\2"+
		"-\u00c9\3\2\2\2/\u00cd\3\2\2\2\61\u00cf\3\2\2\2\63\u00d4\3\2\2\2\65\u00db"+
		"\3\2\2\2\67\u00e0\3\2\2\29\u00e8\3\2\2\2;\u00ea\3\2\2\2=\u00ec\3\2\2\2"+
		"?\u00ee\3\2\2\2A\u00f0\3\2\2\2C\u00f2\3\2\2\2EF\7_\2\2F\4\3\2\2\2GH\7"+
		"d\2\2HI\7q\2\2IJ\7q\2\2JK\7n\2\2K\6\3\2\2\2LM\7h\2\2MN\7w\2\2NO\7p\2\2"+
		"OP\7e\2\2PQ\7v\2\2QR\7k\2\2RS\7q\2\2ST\7p\2\2T\b\3\2\2\2UV\7h\2\2VW\7"+
		"w\2\2WX\7p\2\2XY\7e\2\2YZ\7a\2\2Z[\7e\2\2[\\\7c\2\2\\]\7n\2\2]^\7n\2\2"+
		"^\n\3\2\2\2_`\7y\2\2`a\7j\2\2ab\7k\2\2bc\7n\2\2cd\7g\2\2d\f\3\2\2\2ef"+
		"\7]\2\2f\16\3\2\2\2gh\7k\2\2hi\7h\2\2i\20\3\2\2\2jk\7k\2\2kl\7p\2\2lm"+
		"\7v\2\2m\22\3\2\2\2no\7u\2\2op\7v\2\2pq\7c\2\2qr\7e\2\2rs\7m\2\2st\7\60"+
		"\2\2tu\7v\2\2uv\7q\2\2vw\7r\2\2wx\7*\2\2xy\7+\2\2y\24\3\2\2\2z{\7u\2\2"+
		"{|\7v\2\2|}\7c\2\2}~\7e\2\2~\177\7m\2\2\177\u0080\7\60\2\2\u0080\u0081"+
		"\7r\2\2\u0081\u0082\7q\2\2\u0082\u0083\7r\2\2\u0083\u0084\7*\2\2\u0084"+
		"\u0085\7+\2\2\u0085\26\3\2\2\2\u0086\u0087\7h\2\2\u0087\u0088\7c\2\2\u0088"+
		"\u0089\7e\2\2\u0089\u008a\7v\2\2\u008a\30\3\2\2\2\u008b\u008c\7u\2\2\u008c"+
		"\u008d\7v\2\2\u008d\u008e\7c\2\2\u008e\u008f\7e\2\2\u008f\u0090\7m\2\2"+
		"\u0090\u0091\7\60\2\2\u0091\u0092\7r\2\2\u0092\u0093\7w\2\2\u0093\u0094"+
		"\7u\2\2\u0094\u0095\7j\2\2\u0095\u0096\7*\2\2\u0096\32\3\2\2\2\u0097\u0098"+
		"\7g\2\2\u0098\u0099\7n\2\2\u0099\u009a\7u\2\2\u009a\u009b\7g\2\2\u009b"+
		"\34\3\2\2\2\u009c\u009d\7*\2\2\u009d\36\3\2\2\2\u009e\u009f\7+\2\2\u009f"+
		" \3\2\2\2\u00a0\u00a1\7.\2\2\u00a1\"\3\2\2\2\u00a2\u00a3\7}\2\2\u00a3"+
		"$\3\2\2\2\u00a4\u00a5\7\177\2\2\u00a5&\3\2\2\2\u00a6\u00a7\7K\2\2\u00a7"+
		"\u00a8\7U\2\2\u00a8\u00a9\7G\2\2\u00a9\u00aa\7S\2\2\u00aa\u00ab\7W\2\2"+
		"\u00ab\u00ac\7C\2\2\u00ac\u00c0\7N\2\2\u00ad\u00ae\7P\2\2\u00ae\u00af"+
		"\7Q\2\2\u00af\u00b0\7V\2\2\u00b0\u00b1\7G\2\2\u00b1\u00b2\7S\2\2\u00b2"+
		"\u00b3\7W\2\2\u00b3\u00b4\7C\2\2\u00b4\u00c0\7N\2\2\u00b5\u00b6\7I\2\2"+
		"\u00b6\u00c0\7V\2\2\u00b7\u00b8\7I\2\2\u00b8\u00b9\7V\2\2\u00b9\u00c0"+
		"\7G\2\2\u00ba\u00bb\7N\2\2\u00bb\u00c0\7V\2\2\u00bc\u00bd\7N\2\2\u00bd"+
		"\u00be\7V\2\2\u00be\u00c0\7G\2\2\u00bf\u00a6\3\2\2\2\u00bf\u00ad\3\2\2"+
		"\2\u00bf\u00b5\3\2\2\2\u00bf\u00b7\3\2\2\2\u00bf\u00ba\3\2\2\2\u00bf\u00bc"+
		"\3\2\2\2\u00c0(\3\2\2\2\u00c1\u00c2\7?\2\2\u00c2*\3\2\2\2\u00c3\u00c4"+
		"\7r\2\2\u00c4\u00c5\7t\2\2\u00c5\u00c6\7k\2\2\u00c6\u00c7\7p\2\2\u00c7"+
		"\u00c8\7v\2\2\u00c8,\3\2\2\2\u00c9\u00ca\7$\2\2\u00ca.\3\2\2\2\u00cb\u00ce"+
		"\5\61\31\2\u00cc\u00ce\5\63\32\2\u00cd\u00cb\3\2\2\2\u00cd\u00cc\3\2\2"+
		"\2\u00ce\60\3\2\2\2\u00cf\u00d0\7v\2\2\u00d0\u00d1\7t\2\2\u00d1\u00d2"+
		"\7w\2\2\u00d2\u00d3\7g\2\2\u00d3\62\3\2\2\2\u00d4\u00d5\7h\2\2\u00d5\u00d6"+
		"\7c\2\2\u00d6\u00d7\7n\2\2\u00d7\u00d8\7u\2\2\u00d8\u00d9\7g\2\2\u00d9"+
		"\64\3\2\2\2\u00da\u00dc\t\2\2\2\u00db\u00da\3\2\2\2\u00dc\u00dd\3\2\2"+
		"\2\u00dd\u00db\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\66\3\2\2\2\u00df\u00e1"+
		"\t\3\2\2\u00e0\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e2"+
		"\u00e3\3\2\2\2\u00e38\3\2\2\2\u00e4\u00e9\5;\36\2\u00e5\u00e9\5=\37\2"+
		"\u00e6\u00e9\5? \2\u00e7\u00e9\5A!\2\u00e8\u00e4\3\2\2\2\u00e8\u00e5\3"+
		"\2\2\2\u00e8\u00e6\3\2\2\2\u00e8\u00e7\3\2\2\2\u00e9:\3\2\2\2\u00ea\u00eb"+
		"\7-\2\2\u00eb<\3\2\2\2\u00ec\u00ed\7/\2\2\u00ed>\3\2\2\2\u00ee\u00ef\7"+
		",\2\2\u00ef@\3\2\2\2\u00f0\u00f1\7\61\2\2\u00f1B\3\2\2\2\u00f2\u00f3\t"+
		"\4\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\b\"\2\2\u00f5D\3\2\2\2\b\2\u00bf"+
		"\u00cd\u00dd\u00e2\u00e8\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}