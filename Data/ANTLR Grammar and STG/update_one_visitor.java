import org.stringtemplate.v4.*;
import java.io.*;

public class update_one_visitor extends update_oneBaseVisitor<Integer>{
	
	public static String ic;
	
	@Override 
	public Integer visitPstart(update_oneParser.PstartContext ctx) { 
	
		super.visitPstart(ctx);
		ST temp = new ST("PSTART");
		//System.out.println(temp.render());
		String t = temp.render();
		ic = t;
		ic+="\n";
		return 0;
	}
	
	@Override 
	public Integer visitPend(update_oneParser.PendContext ctx) {
		
		super.visitPend(ctx);
		ST temp = new ST("PEND");
		//System.out.println(temp.render());
		String t = temp.render();
		ic += t;
		try{
			File file = new File("arithmatic_ic.txt");
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(ic);
			output.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		System.out.println(ic);
		return 0; 
		
	}
	
	@Override
	public Integer visitIntDecl(update_oneParser.IntDeclContext ctx) { 
		
		super.visitIntDecl(ctx);
		ST temp = new ST("IDEF <name>,<number>");
		String id = ctx.NAME().getText();
		String no = ctx.NUM().getText();
		temp.add("name", id);
		temp.add("number", no);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	@Override 
	public Integer visitBoolDecl(update_oneParser.BoolDeclContext ctx) { 
	
		super.visitBoolDecl(ctx);
		ST boolDecl = new ST("BDEF <name>,<value>");
		String id = ctx.NAME().getText();
		String value = ctx.VAL().getText();
		boolDecl.add("name", id);
		boolDecl.add("value", value);
		//System.out.println(boolDecl.render());
		String t = boolDecl.render();
		ic += t;
		ic += "\n";
		return 0; 
		
	}
	
	@Override 
	public Integer visitAssg_int(update_oneParser.Assg_intContext ctx) { 
			
		super.visitAssg_int(ctx);
		ST assg_int = new ST("ASSGN <name>,<val>");
		String id = ctx.NAME().getText();
		String val = ctx.NUM().getText();
		assg_int.add("name", id);
		assg_int.add("val", val);
		//System.out.println(assg_int.render());
		String t = assg_int.render();
		ic += t;
		ic += "\n";
		return 0; 
	}
		
	@Override 
	public Integer visitAssg_bool(update_oneParser.Assg_boolContext ctx) { 
	
		super.visitAssg_bool(ctx);
		ST bool_int = new ST("ASSGN <name>,<val>");
		String id = ctx.NAME().getText();
		String val = ctx.VAL().getText();
		bool_int.add("name", id);
		bool_int.add("val", val);
		//System.out.println(bool_int.render());
		String t = bool_int.render();
		ic += t;
		ic += "\n";
		return 0;
	
	}	
	
	@Override 
	public Integer visitPrint(update_oneParser.PrintContext ctx) { 
	
		super.visitPrint(ctx);
		ST print_stmt = new ST("PRINT <name>");
		String id = ctx.NAME().getText();
		print_stmt.add("name", id);
		//System.out.println(print_stmt.render());
		String t = print_stmt.render();
		ic += t;
		ic += "\n";
		return 0; 
		
	}
	
	@Override 
	public Integer visitPrints(update_oneParser.PrintsContext ctx) { 
		
		super.visitPrints(ctx);
		ST print_stmt = new ST("PRINTS <name>");
		String id = ctx.NAME().getText();
		print_stmt.add("name", id);
		//System.out.println(print_stmt.render());
		String t = print_stmt.render();
		ic += t;
		ic += "\n";
		return 0; 
		
	}
	
	
	@Override 
	public Integer visitFnoparam(update_oneParser.FnoparamContext ctx) {
		
		super.visitFnoparam(ctx); 
		ST temp = new ST("DECL <name> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("name", id);
		//System.out.println(temp.render());
		String t = temp.render();
		ic += t;
		ic += "\n";
		return 0; 
		
	}
	
	@Override
	public Integer visitFparam(update_oneParser.FparamContext ctx) { 
	
		super.visitFparam(ctx);
		ST temp = new ST("DECL <name>");
		String id = ctx.NAME().getText();
		temp.add("name", id);
		//System.out.print(temp.render());
		String t = temp.render();
		ic += t;
		//ic += "\n";
		return 0;
		
	}
	
	@Override 
	public Integer visitProgram_no_params(update_oneParser.Program_no_paramsContext ctx) { 
	
		super.visitProgram_no_params(ctx);
		ST temp = new ST("FEND");
		//System.out.println(temp.render());
		String t = temp.render();
		ic += t;
		ic += "\n";
		return 0;
	}
	
	@Override
	public Integer visitProgram_params(update_oneParser.Program_paramsContext ctx) { 
		
		super.visitProgram_params(ctx);
		ST temp = new ST("FEND");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitInteger_parameter(update_oneParser.Integer_parameterContext ctx) {
		
		super.visitInteger_parameter(ctx);
		ST temp = new ST(":int <var> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("var", id);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitBoolean_parameter(update_oneParser.Boolean_parameterContext ctx) { 
		
		super.visitBoolean_parameter(ctx);
		ST temp = new ST(":bool <var> \nFSTART");
		String id = ctx.NAME().getText();
		temp.add("var", id);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitExpr_assg(update_oneParser.Expr_assgContext ctx) { 
		
		super.visitExpr_assg(ctx);
		ST temp = new ST("<result> \n");
		String res = ctx.NAME().getText();
		temp.add("result", res);
		String t = temp.render();
		ic += t;
		//ic += "\n";
		//System.out.print(temp.render());
		return 0; 
	
	}
	
	@Override 
	public Integer visitExpr_num(update_oneParser.Expr_numContext ctx) { 
		
		super.visitExpr_num(ctx);
		ST temp = new ST("<num> ");
		String num = ctx.NUM().getText();
		temp.add("num", num);
		String t = temp.render();
		ic += t;
		//ic += "\n";
		//System.out.print(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitExpression(update_oneParser.ExpressionContext ctx) { 
	
		super.visitExpression(ctx);
		ST temp = new ST("<operation> ");
		String op = ctx.OP().getText();
		if(op.equals("+")){
			op = "ADD";
		}
		else if(op.equals("-")){
			op = "SUB";
		}
		else if(op.equals("*")){
			op = "MUL";
		}
		else{
			op = "DIV";
		}
		temp.add("operation", op);
		String t = temp.render();
		ic += t;
		//ic += "\n";
		//System.out.print(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitIf_main(update_oneParser.If_mainContext ctx) { 
		
		super.visitIf_main(ctx);
		ST temp = new ST("IF");
		String t = temp.render();
		ic += t;
		//ic += "\n";
		//System.out.print(temp.render());
		return 0; 
		
	}
	
	@Override 
	public Integer visitExpr_comp(update_oneParser.Expr_compContext ctx) { 
		
		super.visitExpr_comp(ctx);
		ST temp = new ST(" <var1> <op> <var2>");
		String var1 = ctx.val_one().getText();
		String op = ctx.OPC().getText();
		String var2 = ctx.val_two().getText();
		if(op.equals("ISEQUAL")){
			op = "EQLS";
		}
		else if(op.equals("NOTEQUAL")){
			op = "NEQLS";
		}
		else if(op.equals("LT")){
			op = "LT";
		}
		else if(op.equals("GT")){
			op = "GT";
		}
		else if(op.equals("LTE")){
			op = "LTE";
		}
		else{
			op = "GTE";
		}
		temp.add("var1", var1);
		temp.add("var2", var2);
		temp.add("op", op);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitElse_main(update_oneParser.Else_mainContext ctx) {
		
		super.visitElse_main(ctx);
		ST temp = new ST("ELSE");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitIfelse_stmnt(update_oneParser.Ifelse_stmntContext ctx) { 
		
		super.visitIfelse_stmnt(ctx);
		ST temp = new ST("ENDIF");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	/*@Override
	public Integer visitWhileSt(update_oneParser.WhileStContext ctx) { 
		
		super.visitWhileSt(ctx);
		ST temp = new ST("ENDWHILE");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
		
	}*/
	
	@Override
	public Integer visitWhile_main(update_oneParser.While_mainContext ctx) { 
		
		super.visitWhile_main(ctx);
		ST temp = new ST("WHILE");
		String t = temp.render();
		ic += t;
		//ic += "\n";
		//System.out.print(temp.render());
		return 0; 
		
	}
	
	@Override
	public Integer visitStack_push(update_oneParser.Stack_pushContext ctx) { 
		
		super.visitStack_push(ctx);
		ST temp = new ST("PUSH <num>");
		String id = ctx.NUM().getText();
		temp.add("num", id);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0;
	}
	
	@Override
	public Integer visitStack_pop(update_oneParser.Stack_popContext ctx) { 
		
		super.visitStack_pop(ctx);
		ST temp = new ST("POP");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitStack_top(update_oneParser.Stack_topContext ctx) { 
		
		super.visitStack_top(ctx);
		ST temp = new ST("TOP");
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0; 
	}
	
	@Override
	public Integer visitFunc_call_num(update_oneParser.Func_call_numContext ctx) {
		
		super.visitFunc_call_num(ctx);
		ST temp = new ST("CALL <name> <value>");
		String name = ctx.NAME().getText();
		String value = ctx.NUM().getText();
		temp.add("name", name);
		temp.add("value", value);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0;
	}
	
	@Override
	public Integer visitFunc_call_noparams(update_oneParser.Func_call_noparamsContext ctx) {
		
		super.visitFunc_call_noparams(ctx);
		ST temp = new ST("CALL <name>");
		String name = ctx.NAME().getText();
		temp.add("name", name);
		String t = temp.render();
		ic+=t;
		ic += "\n";
		return 0;
	}
	
	@Override
	public Integer visitFactFunc(update_oneParser.FactFuncContext ctx) { 
		
		super.visitFactFunc(ctx);
		ST temp = new ST("FACT <val>");
		String value = ctx.NUM().getText();
		temp.add("val", value);
		String t = temp.render();
		ic += t;
		ic += "\n";
		//System.out.println(temp.render());
		return 0;
	}
	
	@Override
	public Integer visitProgram_while(update_oneParser.Program_whileContext ctx) { 
		
		super.visitProgram_while(ctx);
		ST temp = new ST("ENDWHILE");
		String t = temp.render();
		ic += t;
		ic += "\n";
		return 0; 
	}
}