grammar update_one;

start : pstart (program)+ pend ;

pstart : '[' ;

pend : ']' ;

program : fparam STARTB parameter (COMMA parameter)* ENDB block  	#program_params  
		| fnoparam STARTB ENDB block								#program_no_params
		| declSt													#program_decl
		| func_call													#program_func_call
		| whileSt													#program_while
		| ifSt														#program_if
		;
		
fparam : 'function' NAME ;

fnoparam : 'function' NAME ;
		
STARTB : '(' ;

ENDB : ')' ;

COMMA : ',' ;
		
parameter : integer NAME				#integer_parameter
			| bOOL NAME					#boolean_parameter
			;

integer : 'int' ;

bOOL : 'bool' ;
			
block : BLOCKSTART (statement)+ BLOCKEND;

BLOCKSTART : '{' ;

BLOCKEND : '}' ;

statement : declSt 
		| assgSt
		|printSt
		|expr_assg
		|stackFunc
		|factFunc
		;

declSt : intDecl
		| boolDecl
		;
			
intDecl : integer NAME EQ NUM ;
		
boolDecl : bOOL NAME EQ VAL;

func_call : 'func_call' NAME '(' NUM ')'		#func_call_num
			| 'func_call' NAME					#func_call_noparams
			;

assgSt : NAME EQ NUM					#assg_int
		|NAME EQ VAL 					#assg_bool
		;

ifSt : if_main expr_comp ')' block else_main block 	#ifelse_stmnt		                      
		;
		
if_main : 'if' '('
	;
		
else_main : 'else' ;

whileSt : while_main expr_comp ')' block ;

while_main : 'while' '(' ;

stackFunc : 'stack.push(' NUM ')'		#stack_push
			| 'stack.top()'				#stack_top
			| 'stack.pop()'				#stack_pop
			;
			
factFunc : 'fact' '(' NUM ')' ;
		
expr_assg : NAME EQ expr ;
		
expr : NUM								#expr_num
		| VAL							#expr_val
		| NAME							#expr_name
		| expr OP expr					#expression
		;
		
expr_comp : val_one OPC val_two
			;

val_one :NUM
		|VAL
		|NAME
		;
		
OPC : 'ISEQUAL'
		|'NOTEQUAL'
		|'GT'
		|'GTE'
		|'LT'
		|'LTE'
		;

val_two : NUM
		|VAL
		|NAME
		;

EQ : '=' ;

printSt : PRINT STARTB NAME ENDB				#print
		| PRINT STARTB QUOTE NAME QUOTE ENDB	#prints
		;
		
PRINT : 'print' ;

QUOTE : '"' ;
		
VAL : TRUE
		| FALSE
		;

TRUE : 'true' ;

FALSE : 'false' ;

		
NUM : [0-9]+ ;		

NAME : [a-zA-Z]+ ;

OP : 	ADD
	| SUB
	| MUL
	| DIV
	;
	

ADD : '+' ;

SUB : '-' ;

MUL : '*' ;

DIV : '/' ;

WS : [ \t\n\r] -> skip ;