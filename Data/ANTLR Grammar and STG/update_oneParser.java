// Generated from update_one.g4 by ANTLR 4.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class update_oneParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__12=1, T__11=2, T__10=3, T__9=4, T__8=5, T__7=6, T__6=7, T__5=8, T__4=9, 
		T__3=10, T__2=11, T__1=12, T__0=13, STARTB=14, ENDB=15, COMMA=16, BLOCKSTART=17, 
		BLOCKEND=18, OPC=19, EQ=20, PRINT=21, QUOTE=22, VAL=23, TRUE=24, FALSE=25, 
		NUM=26, NAME=27, OP=28, ADD=29, SUB=30, MUL=31, DIV=32, WS=33;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "'bool'", "'function'", "'func_call'", "'while'", 
		"'['", "'if'", "'int'", "'stack.top()'", "'stack.pop()'", "'fact'", "'stack.push('", 
		"'else'", "'('", "')'", "','", "'{'", "'}'", "OPC", "'='", "'print'", 
		"'\"'", "VAL", "'true'", "'false'", "NUM", "NAME", "OP", "'+'", "'-'", 
		"'*'", "'/'", "WS"
	};
	public static final int
		RULE_start = 0, RULE_pstart = 1, RULE_pend = 2, RULE_program = 3, RULE_fparam = 4, 
		RULE_fnoparam = 5, RULE_parameter = 6, RULE_integer = 7, RULE_bOOL = 8, 
		RULE_block = 9, RULE_statement = 10, RULE_declSt = 11, RULE_intDecl = 12, 
		RULE_boolDecl = 13, RULE_func_call = 14, RULE_assgSt = 15, RULE_ifSt = 16, 
		RULE_if_main = 17, RULE_else_main = 18, RULE_whileSt = 19, RULE_while_main = 20, 
		RULE_stackFunc = 21, RULE_factFunc = 22, RULE_expr_assg = 23, RULE_expr = 24, 
		RULE_expr_comp = 25, RULE_val_one = 26, RULE_val_two = 27, RULE_printSt = 28;
	public static final String[] ruleNames = {
		"start", "pstart", "pend", "program", "fparam", "fnoparam", "parameter", 
		"integer", "bOOL", "block", "statement", "declSt", "intDecl", "boolDecl", 
		"func_call", "assgSt", "ifSt", "if_main", "else_main", "whileSt", "while_main", 
		"stackFunc", "factFunc", "expr_assg", "expr", "expr_comp", "val_one", 
		"val_two", "printSt"
	};

	@Override
	public String getGrammarFileName() { return "update_one.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public update_oneParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public PstartContext pstart() {
			return getRuleContext(PstartContext.class,0);
		}
		public List<ProgramContext> program() {
			return getRuleContexts(ProgramContext.class);
		}
		public ProgramContext program(int i) {
			return getRuleContext(ProgramContext.class,i);
		}
		public PendContext pend() {
			return getRuleContext(PendContext.class,0);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58); pstart();
			setState(60); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(59); program();
				}
				}
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 3) | (1L << 4) | (1L << 5) | (1L << 7) | (1L << 8))) != 0) );
			setState(64); pend();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PstartContext extends ParserRuleContext {
		public PstartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pstart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterPstart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitPstart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitPstart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PstartContext pstart() throws RecognitionException {
		PstartContext _localctx = new PstartContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_pstart);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PendContext extends ParserRuleContext {
		public PendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterPend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitPend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitPend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PendContext pend() throws RecognitionException {
		PendContext _localctx = new PendContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_pend);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); match(1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	 
		public ProgramContext() { }
		public void copyFrom(ProgramContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Program_paramsContext extends ProgramContext {
		public FparamContext fparam() {
			return getRuleContext(FparamContext.class,0);
		}
		public TerminalNode ENDB() { return getToken(update_oneParser.ENDB, 0); }
		public TerminalNode STARTB() { return getToken(update_oneParser.STARTB, 0); }
		public List<TerminalNode> COMMA() { return getTokens(update_oneParser.COMMA); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(update_oneParser.COMMA, i);
		}
		public Program_paramsContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_params(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_params(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_params(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program_ifContext extends ProgramContext {
		public IfStContext ifSt() {
			return getRuleContext(IfStContext.class,0);
		}
		public Program_ifContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_if(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_if(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_if(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program_no_paramsContext extends ProgramContext {
		public TerminalNode ENDB() { return getToken(update_oneParser.ENDB, 0); }
		public TerminalNode STARTB() { return getToken(update_oneParser.STARTB, 0); }
		public FnoparamContext fnoparam() {
			return getRuleContext(FnoparamContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public Program_no_paramsContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_no_params(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_no_params(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_no_params(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program_declContext extends ProgramContext {
		public DeclStContext declSt() {
			return getRuleContext(DeclStContext.class,0);
		}
		public Program_declContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_decl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program_func_callContext extends ProgramContext {
		public Func_callContext func_call() {
			return getRuleContext(Func_callContext.class,0);
		}
		public Program_func_callContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_func_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_func_call(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_func_call(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program_whileContext extends ProgramContext {
		public WhileStContext whileSt() {
			return getRuleContext(WhileStContext.class,0);
		}
		public Program_whileContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterProgram_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitProgram_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitProgram_while(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_program);
		int _la;
		try {
			setState(92);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				_localctx = new Program_paramsContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(70); fparam();
				setState(71); match(STARTB);
				setState(72); parameter();
				setState(77);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(73); match(COMMA);
					setState(74); parameter();
					}
					}
					setState(79);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(80); match(ENDB);
				setState(81); block();
				}
				break;

			case 2:
				_localctx = new Program_no_paramsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(83); fnoparam();
				setState(84); match(STARTB);
				setState(85); match(ENDB);
				setState(86); block();
				}
				break;

			case 3:
				_localctx = new Program_declContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(88); declSt();
				}
				break;

			case 4:
				_localctx = new Program_func_callContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(89); func_call();
				}
				break;

			case 5:
				_localctx = new Program_whileContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(90); whileSt();
				}
				break;

			case 6:
				_localctx = new Program_ifContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(91); ifSt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FparamContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public FparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterFparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitFparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitFparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FparamContext fparam() throws RecognitionException {
		FparamContext _localctx = new FparamContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_fparam);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); match(3);
			setState(95); match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FnoparamContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public FnoparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fnoparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterFnoparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitFnoparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitFnoparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FnoparamContext fnoparam() throws RecognitionException {
		FnoparamContext _localctx = new FnoparamContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_fnoparam);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97); match(3);
			setState(98); match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
	 
		public ParameterContext() { }
		public void copyFrom(ParameterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Integer_parameterContext extends ParameterContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public Integer_parameterContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterInteger_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitInteger_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitInteger_parameter(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Boolean_parameterContext extends ParameterContext {
		public BOOLContext bOOL() {
			return getRuleContext(BOOLContext.class,0);
		}
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public Boolean_parameterContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterBoolean_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitBoolean_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitBoolean_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_parameter);
		try {
			setState(106);
			switch (_input.LA(1)) {
			case 8:
				_localctx = new Integer_parameterContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(100); integer();
				setState(101); match(NAME);
				}
				break;
			case 2:
				_localctx = new Boolean_parameterContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(103); bOOL();
				setState(104); match(NAME);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerContext extends ParserRuleContext {
		public IntegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitInteger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitInteger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerContext integer() throws RecognitionException {
		IntegerContext _localctx = new IntegerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_integer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BOOLContext extends ParserRuleContext {
		public BOOLContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bOOL; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterBOOL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitBOOL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitBOOL(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BOOLContext bOOL() throws RecognitionException {
		BOOLContext _localctx = new BOOLContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_bOOL);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110); match(2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public TerminalNode BLOCKEND() { return getToken(update_oneParser.BLOCKEND, 0); }
		public TerminalNode BLOCKSTART() { return getToken(update_oneParser.BLOCKSTART, 0); }
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112); match(BLOCKSTART);
			setState(114); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(113); statement();
				}
				}
				setState(116); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 8) | (1L << 9) | (1L << 10) | (1L << 11) | (1L << 12) | (1L << PRINT) | (1L << NAME))) != 0) );
			setState(118); match(BLOCKEND);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StackFuncContext stackFunc() {
			return getRuleContext(StackFuncContext.class,0);
		}
		public FactFuncContext factFunc() {
			return getRuleContext(FactFuncContext.class,0);
		}
		public DeclStContext declSt() {
			return getRuleContext(DeclStContext.class,0);
		}
		public Expr_assgContext expr_assg() {
			return getRuleContext(Expr_assgContext.class,0);
		}
		public AssgStContext assgSt() {
			return getRuleContext(AssgStContext.class,0);
		}
		public PrintStContext printSt() {
			return getRuleContext(PrintStContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_statement);
		try {
			setState(126);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(120); declSt();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(121); assgSt();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(122); printSt();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(123); expr_assg();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(124); stackFunc();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(125); factFunc();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclStContext extends ParserRuleContext {
		public IntDeclContext intDecl() {
			return getRuleContext(IntDeclContext.class,0);
		}
		public BoolDeclContext boolDecl() {
			return getRuleContext(BoolDeclContext.class,0);
		}
		public DeclStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declSt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterDeclSt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitDeclSt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitDeclSt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclStContext declSt() throws RecognitionException {
		DeclStContext _localctx = new DeclStContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_declSt);
		try {
			setState(130);
			switch (_input.LA(1)) {
			case 8:
				enterOuterAlt(_localctx, 1);
				{
				setState(128); intDecl();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(129); boolDecl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntDeclContext extends ParserRuleContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public TerminalNode EQ() { return getToken(update_oneParser.EQ, 0); }
		public IntDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterIntDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitIntDecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitIntDecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntDeclContext intDecl() throws RecognitionException {
		IntDeclContext _localctx = new IntDeclContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_intDecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132); integer();
			setState(133); match(NAME);
			setState(134); match(EQ);
			setState(135); match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolDeclContext extends ParserRuleContext {
		public TerminalNode VAL() { return getToken(update_oneParser.VAL, 0); }
		public BOOLContext bOOL() {
			return getRuleContext(BOOLContext.class,0);
		}
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode EQ() { return getToken(update_oneParser.EQ, 0); }
		public BoolDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterBoolDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitBoolDecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitBoolDecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolDeclContext boolDecl() throws RecognitionException {
		BoolDeclContext _localctx = new BoolDeclContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_boolDecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137); bOOL();
			setState(138); match(NAME);
			setState(139); match(EQ);
			setState(140); match(VAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_callContext extends ParserRuleContext {
		public Func_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_call; }
	 
		public Func_callContext() { }
		public void copyFrom(Func_callContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Func_call_numContext extends Func_callContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public Func_call_numContext(Func_callContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterFunc_call_num(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitFunc_call_num(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitFunc_call_num(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Func_call_noparamsContext extends Func_callContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public Func_call_noparamsContext(Func_callContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterFunc_call_noparams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitFunc_call_noparams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitFunc_call_noparams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_callContext func_call() throws RecognitionException {
		Func_callContext _localctx = new Func_callContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_func_call);
		try {
			setState(149);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new Func_call_numContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(142); match(4);
				setState(143); match(NAME);
				setState(144); match(STARTB);
				setState(145); match(NUM);
				setState(146); match(ENDB);
				}
				break;

			case 2:
				_localctx = new Func_call_noparamsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(147); match(4);
				setState(148); match(NAME);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssgStContext extends ParserRuleContext {
		public AssgStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assgSt; }
	 
		public AssgStContext() { }
		public void copyFrom(AssgStContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Assg_intContext extends AssgStContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public TerminalNode EQ() { return getToken(update_oneParser.EQ, 0); }
		public Assg_intContext(AssgStContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterAssg_int(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitAssg_int(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitAssg_int(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Assg_boolContext extends AssgStContext {
		public TerminalNode VAL() { return getToken(update_oneParser.VAL, 0); }
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode EQ() { return getToken(update_oneParser.EQ, 0); }
		public Assg_boolContext(AssgStContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterAssg_bool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitAssg_bool(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitAssg_bool(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssgStContext assgSt() throws RecognitionException {
		AssgStContext _localctx = new AssgStContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_assgSt);
		try {
			setState(157);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				_localctx = new Assg_intContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(151); match(NAME);
				setState(152); match(EQ);
				setState(153); match(NUM);
				}
				break;

			case 2:
				_localctx = new Assg_boolContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(154); match(NAME);
				setState(155); match(EQ);
				setState(156); match(VAL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStContext extends ParserRuleContext {
		public IfStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifSt; }
	 
		public IfStContext() { }
		public void copyFrom(IfStContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Ifelse_stmntContext extends IfStContext {
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public Expr_compContext expr_comp() {
			return getRuleContext(Expr_compContext.class,0);
		}
		public If_mainContext if_main() {
			return getRuleContext(If_mainContext.class,0);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public Else_mainContext else_main() {
			return getRuleContext(Else_mainContext.class,0);
		}
		public Ifelse_stmntContext(IfStContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterIfelse_stmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitIfelse_stmnt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitIfelse_stmnt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStContext ifSt() throws RecognitionException {
		IfStContext _localctx = new IfStContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_ifSt);
		try {
			_localctx = new Ifelse_stmntContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(159); if_main();
			setState(160); expr_comp();
			setState(161); match(ENDB);
			setState(162); block();
			setState(163); else_main();
			setState(164); block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_mainContext extends ParserRuleContext {
		public If_mainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterIf_main(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitIf_main(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitIf_main(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_mainContext if_main() throws RecognitionException {
		If_mainContext _localctx = new If_mainContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_if_main);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166); match(7);
			setState(167); match(STARTB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_mainContext extends ParserRuleContext {
		public Else_mainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterElse_main(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitElse_main(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitElse_main(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_mainContext else_main() throws RecognitionException {
		Else_mainContext _localctx = new Else_mainContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_else_main);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169); match(13);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStContext extends ParserRuleContext {
		public While_mainContext while_main() {
			return getRuleContext(While_mainContext.class,0);
		}
		public Expr_compContext expr_comp() {
			return getRuleContext(Expr_compContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public WhileStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileSt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterWhileSt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitWhileSt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitWhileSt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStContext whileSt() throws RecognitionException {
		WhileStContext _localctx = new WhileStContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_whileSt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171); while_main();
			setState(172); expr_comp();
			setState(173); match(ENDB);
			setState(174); block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_mainContext extends ParserRuleContext {
		public While_mainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterWhile_main(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitWhile_main(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitWhile_main(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_mainContext while_main() throws RecognitionException {
		While_mainContext _localctx = new While_mainContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_while_main);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176); match(5);
			setState(177); match(STARTB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StackFuncContext extends ParserRuleContext {
		public StackFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stackFunc; }
	 
		public StackFuncContext() { }
		public void copyFrom(StackFuncContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Stack_pushContext extends StackFuncContext {
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public Stack_pushContext(StackFuncContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterStack_push(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitStack_push(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitStack_push(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stack_popContext extends StackFuncContext {
		public Stack_popContext(StackFuncContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterStack_pop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitStack_pop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitStack_pop(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stack_topContext extends StackFuncContext {
		public Stack_topContext(StackFuncContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterStack_top(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitStack_top(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitStack_top(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StackFuncContext stackFunc() throws RecognitionException {
		StackFuncContext _localctx = new StackFuncContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_stackFunc);
		try {
			setState(184);
			switch (_input.LA(1)) {
			case 12:
				_localctx = new Stack_pushContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(179); match(12);
				setState(180); match(NUM);
				setState(181); match(ENDB);
				}
				break;
			case 9:
				_localctx = new Stack_topContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(182); match(9);
				}
				break;
			case 10:
				_localctx = new Stack_popContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(183); match(10);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactFuncContext extends ParserRuleContext {
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public FactFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factFunc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterFactFunc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitFactFunc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitFactFunc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactFuncContext factFunc() throws RecognitionException {
		FactFuncContext _localctx = new FactFuncContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_factFunc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186); match(11);
			setState(187); match(STARTB);
			setState(188); match(NUM);
			setState(189); match(ENDB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_assgContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode EQ() { return getToken(update_oneParser.EQ, 0); }
		public Expr_assgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_assg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpr_assg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpr_assg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpr_assg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_assgContext expr_assg() throws RecognitionException {
		Expr_assgContext _localctx = new Expr_assgContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_expr_assg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(191); match(NAME);
			setState(192); match(EQ);
			setState(193); expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpressionContext extends ExprContext {
		public TerminalNode OP() { return getToken(update_oneParser.OP, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_numContext extends ExprContext {
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public Expr_numContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpr_num(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpr_num(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpr_num(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_nameContext extends ExprContext {
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public Expr_nameContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpr_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpr_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpr_name(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_valContext extends ExprContext {
		public TerminalNode VAL() { return getToken(update_oneParser.VAL, 0); }
		public Expr_valContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpr_val(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpr_val(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpr_val(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(199);
			switch (_input.LA(1)) {
			case NUM:
				{
				_localctx = new Expr_numContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(196); match(NUM);
				}
				break;
			case VAL:
				{
				_localctx = new Expr_valContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(197); match(VAL);
				}
				break;
			case NAME:
				{
				_localctx = new Expr_nameContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(198); match(NAME);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(206);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExpressionContext(new ExprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_expr);
					setState(201);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(202); match(OP);
					setState(203); expr(2);
					}
					} 
				}
				setState(208);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Expr_compContext extends ParserRuleContext {
		public TerminalNode OPC() { return getToken(update_oneParser.OPC, 0); }
		public Val_twoContext val_two() {
			return getRuleContext(Val_twoContext.class,0);
		}
		public Val_oneContext val_one() {
			return getRuleContext(Val_oneContext.class,0);
		}
		public Expr_compContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_comp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterExpr_comp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitExpr_comp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitExpr_comp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_compContext expr_comp() throws RecognitionException {
		Expr_compContext _localctx = new Expr_compContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_expr_comp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209); val_one();
			setState(210); match(OPC);
			setState(211); val_two();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Val_oneContext extends ParserRuleContext {
		public TerminalNode VAL() { return getToken(update_oneParser.VAL, 0); }
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public Val_oneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val_one; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterVal_one(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitVal_one(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitVal_one(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Val_oneContext val_one() throws RecognitionException {
		Val_oneContext _localctx = new Val_oneContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_val_one);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(213);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VAL) | (1L << NUM) | (1L << NAME))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Val_twoContext extends ParserRuleContext {
		public TerminalNode VAL() { return getToken(update_oneParser.VAL, 0); }
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(update_oneParser.NUM, 0); }
		public Val_twoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val_two; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterVal_two(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitVal_two(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitVal_two(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Val_twoContext val_two() throws RecognitionException {
		Val_twoContext _localctx = new Val_twoContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_val_two);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(215);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VAL) | (1L << NUM) | (1L << NAME))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintStContext extends ParserRuleContext {
		public PrintStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_printSt; }
	 
		public PrintStContext() { }
		public void copyFrom(PrintStContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrintsContext extends PrintStContext {
		public TerminalNode QUOTE(int i) {
			return getToken(update_oneParser.QUOTE, i);
		}
		public TerminalNode PRINT() { return getToken(update_oneParser.PRINT, 0); }
		public List<TerminalNode> QUOTE() { return getTokens(update_oneParser.QUOTE); }
		public TerminalNode ENDB() { return getToken(update_oneParser.ENDB, 0); }
		public TerminalNode STARTB() { return getToken(update_oneParser.STARTB, 0); }
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public PrintsContext(PrintStContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterPrints(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitPrints(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitPrints(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrintContext extends PrintStContext {
		public TerminalNode PRINT() { return getToken(update_oneParser.PRINT, 0); }
		public TerminalNode ENDB() { return getToken(update_oneParser.ENDB, 0); }
		public TerminalNode STARTB() { return getToken(update_oneParser.STARTB, 0); }
		public TerminalNode NAME() { return getToken(update_oneParser.NAME, 0); }
		public PrintContext(PrintStContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof update_oneListener ) ((update_oneListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof update_oneVisitor ) return ((update_oneVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintStContext printSt() throws RecognitionException {
		PrintStContext _localctx = new PrintStContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_printSt);
		try {
			setState(227);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new PrintContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(217); match(PRINT);
				setState(218); match(STARTB);
				setState(219); match(NAME);
				setState(220); match(ENDB);
				}
				break;

			case 2:
				_localctx = new PrintsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(221); match(PRINT);
				setState(222); match(STARTB);
				setState(223); match(QUOTE);
				setState(224); match(NAME);
				setState(225); match(QUOTE);
				setState(226); match(ENDB);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 24: return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3#\u00e8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\3\2\3\2\6\2?\n\2\r"+
		"\2\16\2@\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\7\5N\n\5\f\5\16\5"+
		"Q\13\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5_\n\5\3\6\3"+
		"\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\5\bm\n\b\3\t\3\t\3\n\3\n\3"+
		"\13\3\13\6\13u\n\13\r\13\16\13v\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f"+
		"\u0081\n\f\3\r\3\r\5\r\u0085\n\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3"+
		"\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u0098\n\20\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\5\21\u00a0\n\21\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\23\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u00bb\n\27\3\30\3\30\3\30\3\30\3\30\3\31"+
		"\3\31\3\31\3\31\3\32\3\32\3\32\3\32\5\32\u00ca\n\32\3\32\3\32\3\32\7\32"+
		"\u00cf\n\32\f\32\16\32\u00d2\13\32\3\33\3\33\3\33\3\33\3\34\3\34\3\35"+
		"\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u00e6\n\36"+
		"\3\36\2\3\62\37\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62"+
		"\64\668:\2\3\4\2\31\31\34\35\u00e1\2<\3\2\2\2\4D\3\2\2\2\6F\3\2\2\2\b"+
		"^\3\2\2\2\n`\3\2\2\2\fc\3\2\2\2\16l\3\2\2\2\20n\3\2\2\2\22p\3\2\2\2\24"+
		"r\3\2\2\2\26\u0080\3\2\2\2\30\u0084\3\2\2\2\32\u0086\3\2\2\2\34\u008b"+
		"\3\2\2\2\36\u0097\3\2\2\2 \u009f\3\2\2\2\"\u00a1\3\2\2\2$\u00a8\3\2\2"+
		"\2&\u00ab\3\2\2\2(\u00ad\3\2\2\2*\u00b2\3\2\2\2,\u00ba\3\2\2\2.\u00bc"+
		"\3\2\2\2\60\u00c1\3\2\2\2\62\u00c9\3\2\2\2\64\u00d3\3\2\2\2\66\u00d7\3"+
		"\2\2\28\u00d9\3\2\2\2:\u00e5\3\2\2\2<>\5\4\3\2=?\5\b\5\2>=\3\2\2\2?@\3"+
		"\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\5\6\4\2C\3\3\2\2\2DE\7\b\2\2E\5"+
		"\3\2\2\2FG\7\3\2\2G\7\3\2\2\2HI\5\n\6\2IJ\7\20\2\2JO\5\16\b\2KL\7\22\2"+
		"\2LN\5\16\b\2MK\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2"+
		"\2\2RS\7\21\2\2ST\5\24\13\2T_\3\2\2\2UV\5\f\7\2VW\7\20\2\2WX\7\21\2\2"+
		"XY\5\24\13\2Y_\3\2\2\2Z_\5\30\r\2[_\5\36\20\2\\_\5(\25\2]_\5\"\22\2^H"+
		"\3\2\2\2^U\3\2\2\2^Z\3\2\2\2^[\3\2\2\2^\\\3\2\2\2^]\3\2\2\2_\t\3\2\2\2"+
		"`a\7\5\2\2ab\7\35\2\2b\13\3\2\2\2cd\7\5\2\2de\7\35\2\2e\r\3\2\2\2fg\5"+
		"\20\t\2gh\7\35\2\2hm\3\2\2\2ij\5\22\n\2jk\7\35\2\2km\3\2\2\2lf\3\2\2\2"+
		"li\3\2\2\2m\17\3\2\2\2no\7\n\2\2o\21\3\2\2\2pq\7\4\2\2q\23\3\2\2\2rt\7"+
		"\23\2\2su\5\26\f\2ts\3\2\2\2uv\3\2\2\2vt\3\2\2\2vw\3\2\2\2wx\3\2\2\2x"+
		"y\7\24\2\2y\25\3\2\2\2z\u0081\5\30\r\2{\u0081\5 \21\2|\u0081\5:\36\2}"+
		"\u0081\5\60\31\2~\u0081\5,\27\2\177\u0081\5.\30\2\u0080z\3\2\2\2\u0080"+
		"{\3\2\2\2\u0080|\3\2\2\2\u0080}\3\2\2\2\u0080~\3\2\2\2\u0080\177\3\2\2"+
		"\2\u0081\27\3\2\2\2\u0082\u0085\5\32\16\2\u0083\u0085\5\34\17\2\u0084"+
		"\u0082\3\2\2\2\u0084\u0083\3\2\2\2\u0085\31\3\2\2\2\u0086\u0087\5\20\t"+
		"\2\u0087\u0088\7\35\2\2\u0088\u0089\7\26\2\2\u0089\u008a\7\34\2\2\u008a"+
		"\33\3\2\2\2\u008b\u008c\5\22\n\2\u008c\u008d\7\35\2\2\u008d\u008e\7\26"+
		"\2\2\u008e\u008f\7\31\2\2\u008f\35\3\2\2\2\u0090\u0091\7\6\2\2\u0091\u0092"+
		"\7\35\2\2\u0092\u0093\7\20\2\2\u0093\u0094\7\34\2\2\u0094\u0098\7\21\2"+
		"\2\u0095\u0096\7\6\2\2\u0096\u0098\7\35\2\2\u0097\u0090\3\2\2\2\u0097"+
		"\u0095\3\2\2\2\u0098\37\3\2\2\2\u0099\u009a\7\35\2\2\u009a\u009b\7\26"+
		"\2\2\u009b\u00a0\7\34\2\2\u009c\u009d\7\35\2\2\u009d\u009e\7\26\2\2\u009e"+
		"\u00a0\7\31\2\2\u009f\u0099\3\2\2\2\u009f\u009c\3\2\2\2\u00a0!\3\2\2\2"+
		"\u00a1\u00a2\5$\23\2\u00a2\u00a3\5\64\33\2\u00a3\u00a4\7\21\2\2\u00a4"+
		"\u00a5\5\24\13\2\u00a5\u00a6\5&\24\2\u00a6\u00a7\5\24\13\2\u00a7#\3\2"+
		"\2\2\u00a8\u00a9\7\t\2\2\u00a9\u00aa\7\20\2\2\u00aa%\3\2\2\2\u00ab\u00ac"+
		"\7\17\2\2\u00ac\'\3\2\2\2\u00ad\u00ae\5*\26\2\u00ae\u00af\5\64\33\2\u00af"+
		"\u00b0\7\21\2\2\u00b0\u00b1\5\24\13\2\u00b1)\3\2\2\2\u00b2\u00b3\7\7\2"+
		"\2\u00b3\u00b4\7\20\2\2\u00b4+\3\2\2\2\u00b5\u00b6\7\16\2\2\u00b6\u00b7"+
		"\7\34\2\2\u00b7\u00bb\7\21\2\2\u00b8\u00bb\7\13\2\2\u00b9\u00bb\7\f\2"+
		"\2\u00ba\u00b5\3\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bb-"+
		"\3\2\2\2\u00bc\u00bd\7\r\2\2\u00bd\u00be\7\20\2\2\u00be\u00bf\7\34\2\2"+
		"\u00bf\u00c0\7\21\2\2\u00c0/\3\2\2\2\u00c1\u00c2\7\35\2\2\u00c2\u00c3"+
		"\7\26\2\2\u00c3\u00c4\5\62\32\2\u00c4\61\3\2\2\2\u00c5\u00c6\b\32\1\2"+
		"\u00c6\u00ca\7\34\2\2\u00c7\u00ca\7\31\2\2\u00c8\u00ca\7\35\2\2\u00c9"+
		"\u00c5\3\2\2\2\u00c9\u00c7\3\2\2\2\u00c9\u00c8\3\2\2\2\u00ca\u00d0\3\2"+
		"\2\2\u00cb\u00cc\f\3\2\2\u00cc\u00cd\7\36\2\2\u00cd\u00cf\5\62\32\4\u00ce"+
		"\u00cb\3\2\2\2\u00cf\u00d2\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2"+
		"\2\2\u00d1\63\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d3\u00d4\5\66\34\2\u00d4"+
		"\u00d5\7\25\2\2\u00d5\u00d6\58\35\2\u00d6\65\3\2\2\2\u00d7\u00d8\t\2\2"+
		"\2\u00d8\67\3\2\2\2\u00d9\u00da\t\2\2\2\u00da9\3\2\2\2\u00db\u00dc\7\27"+
		"\2\2\u00dc\u00dd\7\20\2\2\u00dd\u00de\7\35\2\2\u00de\u00e6\7\21\2\2\u00df"+
		"\u00e0\7\27\2\2\u00e0\u00e1\7\20\2\2\u00e1\u00e2\7\30\2\2\u00e2\u00e3"+
		"\7\35\2\2\u00e3\u00e4\7\30\2\2\u00e4\u00e6\7\21\2\2\u00e5\u00db\3\2\2"+
		"\2\u00e5\u00df\3\2\2\2\u00e6;\3\2\2\2\17@O^lv\u0080\u0084\u0097\u009f"+
		"\u00ba\u00c9\u00d0\u00e5";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}