// Generated from update_one.g4 by ANTLR 4.2
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link update_oneParser}.
 */
public interface update_oneListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link update_oneParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull update_oneParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull update_oneParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#stack_push}.
	 * @param ctx the parse tree
	 */
	void enterStack_push(@NotNull update_oneParser.Stack_pushContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#stack_push}.
	 * @param ctx the parse tree
	 */
	void exitStack_push(@NotNull update_oneParser.Stack_pushContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_params}.
	 * @param ctx the parse tree
	 */
	void enterProgram_params(@NotNull update_oneParser.Program_paramsContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_params}.
	 * @param ctx the parse tree
	 */
	void exitProgram_params(@NotNull update_oneParser.Program_paramsContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#val_two}.
	 * @param ctx the parse tree
	 */
	void enterVal_two(@NotNull update_oneParser.Val_twoContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#val_two}.
	 * @param ctx the parse tree
	 */
	void exitVal_two(@NotNull update_oneParser.Val_twoContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#assg_int}.
	 * @param ctx the parse tree
	 */
	void enterAssg_int(@NotNull update_oneParser.Assg_intContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#assg_int}.
	 * @param ctx the parse tree
	 */
	void exitAssg_int(@NotNull update_oneParser.Assg_intContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#expr_num}.
	 * @param ctx the parse tree
	 */
	void enterExpr_num(@NotNull update_oneParser.Expr_numContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expr_num}.
	 * @param ctx the parse tree
	 */
	void exitExpr_num(@NotNull update_oneParser.Expr_numContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_decl}.
	 * @param ctx the parse tree
	 */
	void enterProgram_decl(@NotNull update_oneParser.Program_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_decl}.
	 * @param ctx the parse tree
	 */
	void exitProgram_decl(@NotNull update_oneParser.Program_declContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(@NotNull update_oneParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(@NotNull update_oneParser.BlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#func_call_noparams}.
	 * @param ctx the parse tree
	 */
	void enterFunc_call_noparams(@NotNull update_oneParser.Func_call_noparamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#func_call_noparams}.
	 * @param ctx the parse tree
	 */
	void exitFunc_call_noparams(@NotNull update_oneParser.Func_call_noparamsContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#expr_comp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_comp(@NotNull update_oneParser.Expr_compContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expr_comp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_comp(@NotNull update_oneParser.Expr_compContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#declSt}.
	 * @param ctx the parse tree
	 */
	void enterDeclSt(@NotNull update_oneParser.DeclStContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#declSt}.
	 * @param ctx the parse tree
	 */
	void exitDeclSt(@NotNull update_oneParser.DeclStContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#assg_bool}.
	 * @param ctx the parse tree
	 */
	void enterAssg_bool(@NotNull update_oneParser.Assg_boolContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#assg_bool}.
	 * @param ctx the parse tree
	 */
	void exitAssg_bool(@NotNull update_oneParser.Assg_boolContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#factFunc}.
	 * @param ctx the parse tree
	 */
	void enterFactFunc(@NotNull update_oneParser.FactFuncContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#factFunc}.
	 * @param ctx the parse tree
	 */
	void exitFactFunc(@NotNull update_oneParser.FactFuncContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#if_main}.
	 * @param ctx the parse tree
	 */
	void enterIf_main(@NotNull update_oneParser.If_mainContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#if_main}.
	 * @param ctx the parse tree
	 */
	void exitIf_main(@NotNull update_oneParser.If_mainContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#stack_pop}.
	 * @param ctx the parse tree
	 */
	void enterStack_pop(@NotNull update_oneParser.Stack_popContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#stack_pop}.
	 * @param ctx the parse tree
	 */
	void exitStack_pop(@NotNull update_oneParser.Stack_popContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_if}.
	 * @param ctx the parse tree
	 */
	void enterProgram_if(@NotNull update_oneParser.Program_ifContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_if}.
	 * @param ctx the parse tree
	 */
	void exitProgram_if(@NotNull update_oneParser.Program_ifContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#boolDecl}.
	 * @param ctx the parse tree
	 */
	void enterBoolDecl(@NotNull update_oneParser.BoolDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#boolDecl}.
	 * @param ctx the parse tree
	 */
	void exitBoolDecl(@NotNull update_oneParser.BoolDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_no_params}.
	 * @param ctx the parse tree
	 */
	void enterProgram_no_params(@NotNull update_oneParser.Program_no_paramsContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_no_params}.
	 * @param ctx the parse tree
	 */
	void exitProgram_no_params(@NotNull update_oneParser.Program_no_paramsContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void enterWhileSt(@NotNull update_oneParser.WhileStContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void exitWhileSt(@NotNull update_oneParser.WhileStContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#prints}.
	 * @param ctx the parse tree
	 */
	void enterPrints(@NotNull update_oneParser.PrintsContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#prints}.
	 * @param ctx the parse tree
	 */
	void exitPrints(@NotNull update_oneParser.PrintsContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#expr_assg}.
	 * @param ctx the parse tree
	 */
	void enterExpr_assg(@NotNull update_oneParser.Expr_assgContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expr_assg}.
	 * @param ctx the parse tree
	 */
	void exitExpr_assg(@NotNull update_oneParser.Expr_assgContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#ifelse_stmnt}.
	 * @param ctx the parse tree
	 */
	void enterIfelse_stmnt(@NotNull update_oneParser.Ifelse_stmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#ifelse_stmnt}.
	 * @param ctx the parse tree
	 */
	void exitIfelse_stmnt(@NotNull update_oneParser.Ifelse_stmntContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#pend}.
	 * @param ctx the parse tree
	 */
	void enterPend(@NotNull update_oneParser.PendContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#pend}.
	 * @param ctx the parse tree
	 */
	void exitPend(@NotNull update_oneParser.PendContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#while_main}.
	 * @param ctx the parse tree
	 */
	void enterWhile_main(@NotNull update_oneParser.While_mainContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#while_main}.
	 * @param ctx the parse tree
	 */
	void exitWhile_main(@NotNull update_oneParser.While_mainContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(@NotNull update_oneParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(@NotNull update_oneParser.IntegerContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#expr_name}.
	 * @param ctx the parse tree
	 */
	void enterExpr_name(@NotNull update_oneParser.Expr_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expr_name}.
	 * @param ctx the parse tree
	 */
	void exitExpr_name(@NotNull update_oneParser.Expr_nameContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#func_call_num}.
	 * @param ctx the parse tree
	 */
	void enterFunc_call_num(@NotNull update_oneParser.Func_call_numContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#func_call_num}.
	 * @param ctx the parse tree
	 */
	void exitFunc_call_num(@NotNull update_oneParser.Func_call_numContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#integer_parameter}.
	 * @param ctx the parse tree
	 */
	void enterInteger_parameter(@NotNull update_oneParser.Integer_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#integer_parameter}.
	 * @param ctx the parse tree
	 */
	void exitInteger_parameter(@NotNull update_oneParser.Integer_parameterContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#bOOL}.
	 * @param ctx the parse tree
	 */
	void enterBOOL(@NotNull update_oneParser.BOOLContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#bOOL}.
	 * @param ctx the parse tree
	 */
	void exitBOOL(@NotNull update_oneParser.BOOLContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#expr_val}.
	 * @param ctx the parse tree
	 */
	void enterExpr_val(@NotNull update_oneParser.Expr_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#expr_val}.
	 * @param ctx the parse tree
	 */
	void exitExpr_val(@NotNull update_oneParser.Expr_valContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(@NotNull update_oneParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(@NotNull update_oneParser.PrintContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull update_oneParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull update_oneParser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(@NotNull update_oneParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(@NotNull update_oneParser.StartContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#else_main}.
	 * @param ctx the parse tree
	 */
	void enterElse_main(@NotNull update_oneParser.Else_mainContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#else_main}.
	 * @param ctx the parse tree
	 */
	void exitElse_main(@NotNull update_oneParser.Else_mainContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#val_one}.
	 * @param ctx the parse tree
	 */
	void enterVal_one(@NotNull update_oneParser.Val_oneContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#val_one}.
	 * @param ctx the parse tree
	 */
	void exitVal_one(@NotNull update_oneParser.Val_oneContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#stack_top}.
	 * @param ctx the parse tree
	 */
	void enterStack_top(@NotNull update_oneParser.Stack_topContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#stack_top}.
	 * @param ctx the parse tree
	 */
	void exitStack_top(@NotNull update_oneParser.Stack_topContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_while}.
	 * @param ctx the parse tree
	 */
	void enterProgram_while(@NotNull update_oneParser.Program_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_while}.
	 * @param ctx the parse tree
	 */
	void exitProgram_while(@NotNull update_oneParser.Program_whileContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#program_func_call}.
	 * @param ctx the parse tree
	 */
	void enterProgram_func_call(@NotNull update_oneParser.Program_func_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#program_func_call}.
	 * @param ctx the parse tree
	 */
	void exitProgram_func_call(@NotNull update_oneParser.Program_func_callContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#fnoparam}.
	 * @param ctx the parse tree
	 */
	void enterFnoparam(@NotNull update_oneParser.FnoparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#fnoparam}.
	 * @param ctx the parse tree
	 */
	void exitFnoparam(@NotNull update_oneParser.FnoparamContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#pstart}.
	 * @param ctx the parse tree
	 */
	void enterPstart(@NotNull update_oneParser.PstartContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#pstart}.
	 * @param ctx the parse tree
	 */
	void exitPstart(@NotNull update_oneParser.PstartContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#intDecl}.
	 * @param ctx the parse tree
	 */
	void enterIntDecl(@NotNull update_oneParser.IntDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#intDecl}.
	 * @param ctx the parse tree
	 */
	void exitIntDecl(@NotNull update_oneParser.IntDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#fparam}.
	 * @param ctx the parse tree
	 */
	void enterFparam(@NotNull update_oneParser.FparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#fparam}.
	 * @param ctx the parse tree
	 */
	void exitFparam(@NotNull update_oneParser.FparamContext ctx);

	/**
	 * Enter a parse tree produced by {@link update_oneParser#boolean_parameter}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_parameter(@NotNull update_oneParser.Boolean_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link update_oneParser#boolean_parameter}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_parameter(@NotNull update_oneParser.Boolean_parameterContext ctx);
}