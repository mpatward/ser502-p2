// Generated from update_one.g4 by ANTLR 4.2
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link update_oneParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface update_oneVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link update_oneParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull update_oneParser.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#stack_push}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStack_push(@NotNull update_oneParser.Stack_pushContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_params(@NotNull update_oneParser.Program_paramsContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#val_two}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal_two(@NotNull update_oneParser.Val_twoContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#assg_int}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssg_int(@NotNull update_oneParser.Assg_intContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#expr_num}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_num(@NotNull update_oneParser.Expr_numContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_decl(@NotNull update_oneParser.Program_declContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull update_oneParser.BlockContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#func_call_noparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_call_noparams(@NotNull update_oneParser.Func_call_noparamsContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#expr_comp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_comp(@NotNull update_oneParser.Expr_compContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#declSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclSt(@NotNull update_oneParser.DeclStContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#assg_bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssg_bool(@NotNull update_oneParser.Assg_boolContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#factFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactFunc(@NotNull update_oneParser.FactFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#if_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_main(@NotNull update_oneParser.If_mainContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#stack_pop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStack_pop(@NotNull update_oneParser.Stack_popContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_if(@NotNull update_oneParser.Program_ifContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#boolDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolDecl(@NotNull update_oneParser.BoolDeclContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_no_params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_no_params(@NotNull update_oneParser.Program_no_paramsContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#whileSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileSt(@NotNull update_oneParser.WhileStContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#prints}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrints(@NotNull update_oneParser.PrintsContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#expr_assg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_assg(@NotNull update_oneParser.Expr_assgContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#ifelse_stmnt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfelse_stmnt(@NotNull update_oneParser.Ifelse_stmntContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#pend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPend(@NotNull update_oneParser.PendContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#while_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_main(@NotNull update_oneParser.While_mainContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#integer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(@NotNull update_oneParser.IntegerContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#expr_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_name(@NotNull update_oneParser.Expr_nameContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#func_call_num}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_call_num(@NotNull update_oneParser.Func_call_numContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#integer_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger_parameter(@NotNull update_oneParser.Integer_parameterContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#bOOL}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL(@NotNull update_oneParser.BOOLContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#expr_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_val(@NotNull update_oneParser.Expr_valContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(@NotNull update_oneParser.PrintContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(@NotNull update_oneParser.StatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(@NotNull update_oneParser.StartContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#else_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_main(@NotNull update_oneParser.Else_mainContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#val_one}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal_one(@NotNull update_oneParser.Val_oneContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#stack_top}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStack_top(@NotNull update_oneParser.Stack_topContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_while(@NotNull update_oneParser.Program_whileContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#program_func_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_func_call(@NotNull update_oneParser.Program_func_callContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#fnoparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnoparam(@NotNull update_oneParser.FnoparamContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#pstart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPstart(@NotNull update_oneParser.PstartContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#intDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntDecl(@NotNull update_oneParser.IntDeclContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#fparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFparam(@NotNull update_oneParser.FparamContext ctx);

	/**
	 * Visit a parse tree produced by {@link update_oneParser#boolean_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_parameter(@NotNull update_oneParser.Boolean_parameterContext ctx);
}