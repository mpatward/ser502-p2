package ser502.runtime;

import java.util.HashMap;

public class Functions {
	public String functionName;
	public HashMap<String,String> functionParameters;
	public String functionBody;
	
	
	public synchronized String getFunctionName() {
		return functionName;
	}
	public synchronized void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public synchronized HashMap<String, String> getFunctionParameters() {
		return functionParameters;
	}
	public synchronized void setFunctionParameters(
			HashMap<String, String> functionParameters) {
		this.functionParameters = functionParameters;
	}
	public synchronized String getFunctionBody() {
		return functionBody;
	}
	public synchronized void setFunctionBody(String functionBody) {
		this.functionBody = functionBody;
	}
}
