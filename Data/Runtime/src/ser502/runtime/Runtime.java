package ser502.runtime;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

/**
 * @author mandar
 *
 */
public class Runtime {
	/**
	 * @param args
	 */
	static Stack stack=new Stack<String>();
	static boolean funcFlag=false,whileFlag=false,funcCallRunning=false,youAreInIF=false,youAreInElse=false,elseEncountered=false,endifEncountered=false,skip=false;	
	public static HashMap symbolTable= new HashMap<String, String>();
	public static Functions function;
	public static String functionBody,functionName,whileBody,whileFirstVar,whileCondition,whileThirdVar;
	public static int factorial ( int input ){
	  int x, fact = 1;
	  for ( x = input; x > 1; x--)
	     fact *= x;

	  return fact;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file=new File(".\\src\\InputFile.txt");
		try {
			Scanner scan=new Scanner(file);
			while(scan.hasNextLine()){
				String lineText=scan.nextLine();
				//System.out.println(lineText);
				String array[]=lineText.split(" ");
					if(array[0].contentEquals("ENDIF")){
						endifEncountered=true;
						skip=false;
					}					
					if(array[0].contentEquals("ELSE")){
						elseEncountered=true;
					}
					
					if(youAreInIF==true && elseEncountered==false){
						//Runtime.parseGlobalTokens(array);		
						skip=false;
					}else if(youAreInIF==true && elseEncountered==true && endifEncountered==false){
						skip=true;
					}else if(youAreInElse==true && elseEncountered==false && endifEncountered==false){
						skip=true;
					}else if(youAreInElse==true && elseEncountered==true && endifEncountered==false){
						//Runtime.parseGlobalTokens(array);
						skip=false;
					}
				if(array.length>0 && skip==false){
					if(array[0].contentEquals("DECL")){
						functionBody="";
						//functionBody=functionBody+lineText+"\n";
						//save function body parameters and store the variables.
						Runtime.parseFunctionDeclTokens(array);
						funcFlag=true;
					}else if(!array[0].equals("FEND") && funcFlag==true){
						functionBody=functionBody+lineText+"\n";
					}else if(array[0].equals("FEND")){
						functionBody=functionBody+lineText;
						funcFlag=false;
						function.setFunctionBody(functionBody);
						symbolTable.put(function.functionName,function);
					}else if(array[0].contentEquals("CALL")){
						funcCallRunning=true;
						Runtime.parseFunctionCallTokens(array);
					}else if(array[0].contentEquals("WHILE")){
						whileBody="";
						whileFirstVar=array[1];
						whileCondition=array[2];
						whileThirdVar=array[3];
						//Runtime.parseWhileLoop(array);
						whileFlag=true;
					}else if(!array[0].equals("ENDWHILE") && whileFlag==true){
						whileBody=whileBody+lineText+"\n"; 
					}else if(array[0].equals("ENDWHILE")){
						Runtime.parseWhileLoop(array);
					}else{
						Runtime.parseGlobalTokens(array);
					}
				}				
			}
			scan.close();
		}catch (FileNotFoundException e) {
			System.out.println("File Not Found.");
		}
	}
	
	public static void parseFunctionCallTokens(String array[]){
		if(array.length>0)
		{
			Functions functionObj=(Functions) symbolTable.get(array[1]);
			HashMap<String,String> tempMap=functionObj.getFunctionParameters();
			tempMap.put("a",array[2]);
			functionObj.setFunctionParameters(tempMap);
			String functionRun[]= functionObj.functionBody.split("\n");
			int functionBodyLength=functionRun.length;
			for(int i=0;i<functionBodyLength;i++){
				String arrayOfTokens[]=functionRun[i].split(" ");
				Runtime.parseGlobalTokens(arrayOfTokens);
			}
			funcCallRunning=false;
		}
	}
	
	public static void parseWhileLoop(String array[]){
		if(array.length>0){
			String whileBodyArray[]=whileBody.split("\n");
			String firstValue=(String)symbolTable.get(whileFirstVar);
			String secondValue=whileThirdVar;		
			while(!firstValue.contentEquals(secondValue)){
				for(int i=0;i<whileBodyArray.length;i++){
					String arrayOfTokens[]=whileBodyArray[i].split(" ");
					Runtime.parseGlobalTokens(arrayOfTokens);
				}
				firstValue=(String)symbolTable.get(whileFirstVar);			
			}
		}
	}
	
	public static void parseFunctionDeclTokens(String array[]) {
		if(array.length>0)
		{			
			if(array[0].contentEquals("DECL")){
				String funcNameValue []= array[1].split(":");
				System.out.println(funcNameValue[0]);
				function=new Functions();
				function.setFunctionName(funcNameValue[0]);
				HashMap<String, String> funcParameteres = new HashMap<String, String>();
				if(funcNameValue[1].equals("INT")){
					funcParameteres.put(array[2],"0");
				}else if(funcNameValue[1].equals("BOOL")){
					funcParameteres.put(array[2],"false");
				}				
				function.setFunctionParameters(funcParameteres);
			}
		}		
	}
	
	public static void parseGlobalTokens(String array[]){
		//System.out.println(Arrays.toString(array));
		if(array.length>0)
		{
			//System.out.println("parseGlobalTokens");
			if(array[0].contentEquals("IDEF")){
				
				String nameValue []= array[1].split(",");
				if(funcCallRunning==true){
					HashMap<String ,String> funcMap=function.getFunctionParameters();
					funcMap.put(nameValue[0],nameValue[1]);
					function.setFunctionParameters(funcMap);
				}else{
					symbolTable.put(nameValue[0], nameValue[1]);
				}				
			}else if(array[0].contentEquals("PRINT")){
				if(funcCallRunning==true){
					HashMap<String ,String> funcMap=function.getFunctionParameters();
					System.out.println(funcMap.get(array[1]));					
				}else{
					System.out.println(Runtime.symbolTable.get(array[1]));
				}				
			}else if(array[0].contentEquals("FACT")){
				System.out.println(factorial(Integer.parseInt(array[1])));
			}else if(array[0].contentEquals("PUSH")){
				stack.push(array[1]);	
			}else if(array[0].contentEquals("TOP")){
				System.out.println(stack.peek());	
			}else if(array[0].contentEquals("POP")){
				System.out.println(stack.pop());	
			}else if(array[0].contentEquals("ASSGN")){
				String nameValue []= array[1].split(",");
				//String temp=nameValue[0];
				//symbolTable.put(nameValue[0], nameValue[1]);
				if(funcCallRunning==true){
					HashMap<String ,String> funcMap=function.getFunctionParameters();
					funcMap.put(nameValue[0],nameValue[1]);
					function.setFunctionParameters(funcMap);
				}else{
					symbolTable.put(nameValue[0], nameValue[1]);
				}
			}else if(array.length>2){
					 if(array[2].contentEquals("ADD")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(funcMap.get(array[1]))+Integer.parseInt(array[0]);
							funcMap.put(array[3], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							if(symbolTable.get(array[1])!=null){
								String in=(String) symbolTable.get(array[1]);
								Integer result=Integer.parseInt(in)+Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}else{
								Integer result=Integer.parseInt(array[0])+Integer.parseInt(array[1]);
								symbolTable.put(array[3], result.toString());
							}
						}				
					}else if(array[1].contentEquals("ADD")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(array[0])+Integer.parseInt((String)funcMap.get(array[2]));
							funcMap.put(array[2], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							Integer result=Integer.parseInt(array[0])+Integer.parseInt((String)symbolTable.get(array[2]));
							symbolTable.put(array[2], result.toString());
							
						}				
					}else if(array[2].contentEquals("SUB")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(funcMap.get(array[1]))-Integer.parseInt(array[0]);
							funcMap.put(array[3], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							if(symbolTable.get(array[1])!=null){
								String in=(String) symbolTable.get(array[1]);
								Integer result=Integer.parseInt(in)-Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}else{
								Integer result=Integer.parseInt(array[1])-Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}
						}				
					}else if(array[1].contentEquals("SUB")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt((String)funcMap.get(array[2]))-Integer.parseInt(array[0]);
							funcMap.put(array[2], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							Integer result=Integer.parseInt((String)symbolTable.get(array[2]))-Integer.parseInt(array[0]);
							symbolTable.put(array[2], result.toString());
						}				
					}else if(array[2].contentEquals("MUL")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(funcMap.get(array[1]))*Integer.parseInt(array[0]);
							funcMap.put(array[3], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							if(symbolTable.get(array[1])!=null){
								String in=(String) symbolTable.get(array[1]);
								Integer result=Integer.parseInt(in)*Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}else{
								Integer result=Integer.parseInt(array[0])*Integer.parseInt(array[1]);
								symbolTable.put(array[3], result.toString());
							}
						}				
					}else if(array[1].contentEquals("MUL")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(array[0])*Integer.parseInt((String)funcMap.get(array[2]));
							funcMap.put(array[2], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							Integer result=Integer.parseInt(array[0])*Integer.parseInt((String)symbolTable.get(array[2]));
							symbolTable.put(array[2], result.toString());
							
						}				
					}else if(array[2].contentEquals("DIV")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt(funcMap.get(array[1]))/Integer.parseInt(array[0]);
							funcMap.put(array[3], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							if(symbolTable.get(array[1])!=null){
								String in=(String) symbolTable.get(array[1]);
								Integer result=Integer.parseInt(in)/Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}else{
								Integer result=Integer.parseInt(array[1])/Integer.parseInt(array[0]);
								symbolTable.put(array[3], result.toString());
							}
						}				
					}else if(array[1].contentEquals("DIV")){
						if(funcCallRunning==true){
							HashMap<String ,String> funcMap=function.getFunctionParameters();
							Integer result=Integer.parseInt((String)funcMap.get(array[2]))/Integer.parseInt(array[0]);
							funcMap.put(array[2], result.toString());
							function.setFunctionParameters(funcMap);
						}else{
							Integer result=Integer.parseInt((String)symbolTable.get(array[2]))/Integer.parseInt(array[0]);
							symbolTable.put(array[2], result.toString());
							
						}				
					}else if(array.length==4){
						if(array[0].contentEquals("IF")){
							if(array[2].contentEquals("EQLS")){
								if(symbolTable.get(array[1]).equals(array[3])){								
									youAreInIF=true;						
								}else{
									youAreInElse=true;
								}
							}
						}
					}
			}
		}
	}
}
